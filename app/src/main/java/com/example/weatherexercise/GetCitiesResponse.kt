package com.example.weatherexercise

data class GetCitiesResponse(val cities: List<City>)