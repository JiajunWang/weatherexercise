package com.example.weatherexercise

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils.isEmpty
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CityWeatherDetailActivity: AppCompatActivity() {
    private lateinit var cityNameTextView: TextView
    private lateinit var cityWeatherTextView: TextView
    private lateinit var cityTemperatureTextView: TextView
    private lateinit var cityFeelingTextView: TextView
    private lateinit var cityPredictTextView: TextView
    private lateinit var cityHumidityTextView: TextView

    private var disposable: Disposable? = null
    private val weatherClient = WeatherClientImpl()

    companion object {
        const val KEY_CITY_NAME = "key_city_name"

        @JvmStatic
        fun startDetailActivity(context: Context, cityName: String) {
            val intent = Intent(context, CityWeatherDetailActivity::class.java).apply {
                putExtra(KEY_CITY_NAME, cityName)
            }
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_weather_detail)
        initView()
        disposable = Observable.fromCallable {
            val cityName = parseCityName()
            if (isEmpty(cityName)) {
                null
            } else {
                weatherClient.getWeatherForCity(cityName!!)
            }
        }
            .subscribeOn(Schedulers.io())
            .observeOn(mainThread())
            .subscribe{
                response -> updateView(response?.currentWeather)
            }
    }

    private fun parseCityName() : String? {
        return intent.getStringExtra(KEY_CITY_NAME)
    }

    private fun initView() {
        cityNameTextView = findViewById(R.id.detail_city_name)
        cityWeatherTextView = findViewById(R.id.detail_city_weather)
        cityTemperatureTextView = findViewById(R.id.detail_city_temperature)
        cityFeelingTextView = findViewById(R.id.detail_city_feeling)
        cityPredictTextView = findViewById(R.id.detail_city_predict)
        cityHumidityTextView = findViewById(R.id.detail_city_humidity)
    }

    private fun updateView(currentWeather: CurrentWeather?) {
        cityNameTextView.text = parseCityName()
        cityWeatherTextView.text = currentWeather?.windDirection
        cityTemperatureTextView.text = String.format("%f °", currentWeather?.feelsLikeCelsius)
        cityFeelingTextView.text = String.format("Feels like %f °", currentWeather?.feelsLikeCelsius)
        cityPredictTextView.text = String.format("%d chance of rain", currentWeather?.rainChancePercent)
        cityHumidityTextView.text = String.format("%d humidity", currentWeather?.rainChancePercent)
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }
}
