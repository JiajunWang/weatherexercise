package com.example.weatherexercise

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CityListAdapter(listener: ClickListener) : RecyclerView.Adapter<CityListAdapter.ViewHolder>() {
    private var cities : MutableList<City> = mutableListOf()
    private val listener = listener

    fun setCityList(cities : List<City>) {
        this.cities.clear()
        this.cities.addAll(cities)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_city, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return this.cities.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameView.text = cities[position].name
        holder.itemView.setOnClickListener { listener.onCityItemClicked(cities[position].name)}
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameView: TextView = itemView.findViewById(R.id.list_item_city_name)
    }

    interface ClickListener {
        fun onCityItemClicked(cityName: String)
    }
}