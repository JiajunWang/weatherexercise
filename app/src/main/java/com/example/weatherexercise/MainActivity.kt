package com.example.weatherexercise

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers.io

class MainActivity : AppCompatActivity(), CityListAdapter.ClickListener {
    private lateinit var recyclerView: RecyclerView

    private val weatherClient = WeatherClientImpl()

    private var disposable : Disposable? = null
    private var cityListAdapter: CityListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cityListAdapter = CityListAdapter(this)
        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.adapter = cityListAdapter

        disposable = Observable.fromCallable { weatherClient.cities }
            .subscribeOn(io())
            .observeOn(mainThread())
            .subscribe{response -> cityListAdapter?.setCityList(response.cities)}
    }

    override fun onDestroy() {
        disposable?.dispose()
        super.onDestroy()
    }

    override fun onCityItemClicked(cityName: String) {
        CityWeatherDetailActivity.startDetailActivity(this, cityName)
    }
}
