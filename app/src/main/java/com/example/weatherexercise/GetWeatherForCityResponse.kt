package com.example.weatherexercise

class GetWeatherForCityResponse(
    val currentWeather: CurrentWeather,
    val predictedWeatherForDays: Map<String, PredictedWeather>)
