package com.example.weatherexercise

data class PredictedWeather(val minTempCelsius: Float, val maxTempCelsius: Float, val shortDescription: String)
