package com.example.weatherexercise

import java.io.IOException

/**
 * Makes network requests to fetch weather data.
 */
interface WeatherClient {

    val cities: GetCitiesResponse

    @Throws(IOException::class)
    fun getWeatherForCity(cityName: String): GetWeatherForCityResponse
}