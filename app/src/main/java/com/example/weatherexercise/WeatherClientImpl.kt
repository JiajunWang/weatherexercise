package com.example.weatherexercise

import android.util.Log
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class WeatherClientImpl : WeatherClient {

    override val cities: GetCitiesResponse
        @Throws(IOException::class)
        get() {
            randomDelay()
            maybeThrowError()
            val cities = ArrayList<City>()
            for ((key, value) in COUNTRIES_WITH_CITIES) {
                for (cityName in value) {
                    cities.add(City(
                        cityName,
                        key,
                        90 * RANDOM.nextGaussian(),
                        180 * RANDOM.nextGaussian()))
                }
            }
            Collections.shuffle(cities)
            return GetCitiesResponse(cities)
        }

    @Throws(IOException::class)
    override fun getWeatherForCity(cityName: String): GetWeatherForCityResponse {
        randomDelay()
        maybeThrowError()
        return GetWeatherForCityResponse(
            CurrentWeather(
                WEATHER_DESCRIPTIONS[RANDOM.nextInt(WEATHER_DESCRIPTIONS.size)],
                randomTemperatureCelsius(),
                (100 * RANDOM.nextFloat()).toShort(),
                (100 * RANDOM.nextFloat()).toShort(),
                (1000 + 20 * RANDOM.nextGaussian()).toString() + " mbar",
                WIND_DIRECTIONS[RANDOM.nextInt(WIND_DIRECTIONS.size)],
                100 * RANDOM.nextFloat()),
            object : HashMap<String, PredictedWeather>() {
                init {
                    put("Sunday", randomPredictedWeather())
                    put("Monday", randomPredictedWeather())
                    put("Tuesday", randomPredictedWeather())
                    put("Wednesday", randomPredictedWeather())
                    put("Thursday", randomPredictedWeather())
                    put("Friday", randomPredictedWeather())
                    put("Saturday", randomPredictedWeather())
                }
            }
        )
    }

    companion object {

        private val RANDOM = Random()

        private val COUNTRIES_WITH_CITIES = HashMap<String, List<String>>()

        init {
            COUNTRIES_WITH_CITIES["Australia"] = Arrays.asList("Canberra", "Melbourne", "Perth", "Sydney")
            COUNTRIES_WITH_CITIES["Canada"] = Arrays.asList("Toronto", "Vancouver", "Montreal", "Calgary")
            COUNTRIES_WITH_CITIES["Ireland"] = Arrays.asList("Dublin", "Galway", "Kilkenny")
            COUNTRIES_WITH_CITIES["Mexico"] = Arrays.asList("Guadalajara", "Mexico City", "Monterrey")
            COUNTRIES_WITH_CITIES["Philippines"] = Arrays.asList("Manila", "Cebu City")
            COUNTRIES_WITH_CITIES["United Kingdom"] = Arrays.asList("London", "Conwy", "Brighton", "Bath", "York", "Edinburgh")
            COUNTRIES_WITH_CITIES["United States"] = Arrays.asList("San Francisco", "New York", "Boston", "Los Angeles")
            COUNTRIES_WITH_CITIES["Uruguay"] = Arrays.asList("Montevideo", "Punta del Este", "Colonia del Sacramento")
        }

        private val WEATHER_DESCRIPTIONS = Arrays.asList(
            "Cloudy with a chance of meatballs",
            "Mostly sunny with afternoon tornadoes",
            "Plague of locusts",
            "Hot as an oven",
            "Sideways rain"
        )

        private val WIND_DIRECTIONS = Arrays.asList(
            "North",
            "Northeast",
            "East",
            "Southeast",
            "South",
            "Southwest",
            "West",
            "Northwest"
        )

        private fun randomPredictedWeather(): PredictedWeather {
            val minTemp = randomTemperatureCelsius()
            val maxTemp = minTemp + 15 * RANDOM.nextFloat()
            return PredictedWeather(
                minTemp,
                maxTemp,
                WEATHER_DESCRIPTIONS[RANDOM.nextInt(WEATHER_DESCRIPTIONS.size)])
        }

        private fun randomTemperatureCelsius(): Float {
            return (10 + 20 * RANDOM.nextGaussian()).toFloat()
        }

        private fun randomDelay() {
            try {
                Thread.sleep(RANDOM.nextInt(5000).toLong())
            } catch (e: InterruptedException) {
                Log.e(WeatherClientImpl::class.java!!.getSimpleName(), "Interrupted sleep")
                Thread.currentThread().interrupt()
            }

        }

        @Throws(IOException::class)
        private fun maybeThrowError() {
            if (true) {
                return
            }
            if (RANDOM.nextInt(5) == 0) {
                throw IOException("fake network error")
            }
        }
    }
}
