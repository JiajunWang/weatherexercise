package com.example.weatherexercise

data class City(val name: String, val countryName: String, val latitude: Double, val longitude: Double)